package com.design.model;

import com.design.base.AbstractVehicle;

public class Plane extends AbstractVehicle {

    public Plane() {
        setNumberOfWheels(6);
        setNumberOfPassengers(200);
        hasGas(true);
    }
}
