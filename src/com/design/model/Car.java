package com.design.model;

import com.design.base.AbstractVehicle;

public class Car extends AbstractVehicle {

    public Car() {
        setNumberOfWheels(4);
        setNumberOfPassengers(4);
        hasGas(true);
    }
}
