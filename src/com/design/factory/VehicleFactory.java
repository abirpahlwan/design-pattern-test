package com.design.factory;

import com.design.base.Vehicle;
import com.design.base.VehicleType;
import com.design.model.Car;
import com.design.model.Plane;

public class VehicleFactory {
    public static Vehicle createVehicle(VehicleType vehicleType){
        switch (vehicleType){
            case CAR:
                return new Car();
            case PLANE:
                return new Plane();
            default:
                return null;
        }
    }
}
