package com.design.base;

public interface Vehicle {
    void setNumberOfWheels(int numberOfWheels);
    void setNumberOfPassengers(int numberOfPassengers);
    void hasGas(boolean gasStatus);
}
