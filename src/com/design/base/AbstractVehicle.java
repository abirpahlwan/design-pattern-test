package com.design.base;

public abstract class AbstractVehicle implements Vehicle {
    private int numberOfWheels;
    private int numberOfPassengers;
    private boolean gasStatus;

    public int getNumberOfWheels(){
        return numberOfWheels;
    }

    @Override
    public void setNumberOfWheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
    }

    public int getNumberOfPassengers(){
        return numberOfPassengers;
    }

    @Override
    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public boolean getGasStatus(){
        return gasStatus;
    }

    @Override
    public void hasGas(boolean gasStatus) {
        this.gasStatus = gasStatus;
    }
}
