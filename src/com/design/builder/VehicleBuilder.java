package com.design.builder;

public interface VehicleBuilder{

    CustomVehicleBuilder withNumberOfWheels(int numberOfWheels);
    CustomVehicleBuilder withNumberOfPassengers(int numberOfPassengers);
    CustomVehicleBuilder withGas(boolean gasStatus);

    CustomVehicle build();
}
