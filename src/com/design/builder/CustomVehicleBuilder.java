package com.design.builder;

public class CustomVehicleBuilder implements VehicleBuilder {

    private CustomVehicle vehicle;

    public CustomVehicleBuilder() {
        vehicle = new CustomVehicle();
    }

    @Override
    public CustomVehicleBuilder withNumberOfWheels(int numberOfWheels) {
        this.vehicle.setNumberOfWheels(numberOfWheels);
        return this;
    }

    @Override
    public CustomVehicleBuilder withNumberOfPassengers(int numberOfPassengers) {
        this.vehicle.setNumberOfPassengers(numberOfPassengers);
        return this;
    }

    @Override
    public CustomVehicleBuilder withGas(boolean gasStatus) {
        this.vehicle.hasGas(gasStatus);
        return this;
    }

    @Override
    public CustomVehicle build() {
        return this.vehicle;
    }
}
