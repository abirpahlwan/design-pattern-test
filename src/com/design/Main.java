package com.design;

import com.design.base.VehicleType;
import com.design.builder.CustomVehicleBuilder;
import com.design.builder.CustomVehicle;
import com.design.factory.VehicleFactory;
import com.design.model.Car;
import com.design.model.Plane;

public class Main {

    public static void main(String[] args) {
        Car car = (Car) VehicleFactory.createVehicle(VehicleType.CAR);
        System.out.println("The Car has " + car.getNumberOfWheels() + " wheels");
        System.out.println("The Car has " + car.getNumberOfPassengers() + " passenger(s)");

        Plane plane = (Plane) VehicleFactory.createVehicle(VehicleType.PLANE);
        System.out.println("The Plane has " + plane.getNumberOfWheels() + " wheels");
        System.out.println("The Plane has " + plane.getNumberOfPassengers() + " passenger(s)");


        CustomVehicle customCar = new CustomVehicleBuilder().withNumberOfWheels(8).withNumberOfPassengers(10).withGas(true).build();
        System.out.println("The Custom Car has " + customCar.getNumberOfWheels() + " wheels");

        CustomVehicle customRocket = new CustomVehicleBuilder().withNumberOfWheels(0).withNumberOfPassengers(1).withGas(true).build();
        System.out.println("The Custom Rocket has " + customRocket.getNumberOfPassengers() + " passenger(s)");
    }
}
